#!/usr/bin/env bash

echo ------------------------------------------------------
echo        Droit sur Apache user : www-data
sudo mkdir -p /var/log/qgis/cpp_server/cache/
sudo touch /var/log/qgis/cpp_server/qgisserver.log
sudo chown -R www-data:www-data /var/log/qgis/*
mkdir ${HOME}/qgisserverdb
sudo chown -R www-data:www-data ${HOME}/qgisserverdb
echo ------------------------------------------------------

echo ------------------------------------------------------
echo        Restart service Apache
sudo service apache2 restart
echo ------------------------------------------------------