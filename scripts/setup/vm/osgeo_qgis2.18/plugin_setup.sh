#!/usr/bin/env bash

echo ------------------------------------------------------
echo             UPDATE mise à jour des paquets
echo ------------------------------------------------------
sudo apt-get update
echo ------------------------------------------------------
echo                    INSTALL libqgis-*
echo ------------------------------------------------------
sudo apt-get -y install libqgis-*
echo ------------------------------------------------------
echo                    INSTALL libqt4-*
echo ------------------------------------------------------
sudo apt-get -y install libqt4-*
echo ------------------------------------------------------
echo                    INSTALL python-sparqlwrapper
echo ------------------------------------------------------
sudo apt-get -y install python-sparqlwrapper
echo ------------------------------------------------------
echo                    INSTALL python3-pyqt4
echo ------------------------------------------------------
sudo apt-get -y install python3-pyqt4
echo ------------------------------------------------------
echo                    INSTALL apache2 libapache2-mod-fcgid
echo ------------------------------------------------------
sudo apt-get -y install apache2 libapache2-mod-fcgid
echo ------------------------------------------------------
echo                    INSTALL qgis-server python-qgis
echo ------------------------------------------------------
sudo apt-get -y install qgis-server python-qgis
echo ------------------------------------------------------
echo                    INSTALL python tools
echo ------------------------------------------------------
sudo apt-get -y install python-pytest
sudo apt-get -y install flake8
sudo pip install rdflib
sduo apt install virtualenv
sudo pip install --user virtualenv



echo ------------------------------------------------------
echo        Apache configuration - Création /etc/apache2/conf-available/qgis-server-port.conf
echo ------------------------------------------------------
sudo rm /tmp/qgis-server-port.conf
echo 'Listen 8080
' >> /tmp/qgis-server-port.conf
sudo mv /tmp/qgis-server-port.conf /etc/apache2/conf-available/qgis-server-port.conf


echo ------------------------------------------------------
echo         Apache configuration - Création /etc/apache2/sites-available/00x-qgis-server.conf
echo ------------------------------------------------------
sudo rm /tmp/001-qgis-server.conf
sudo rm /tmp/002-qgis-server.conf
echo '<VirtualHost *:8080>
  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/html


  ErrorLog ${APACHE_LOG_DIR}/qgis-server-error.log
  CustomLog ${APACHE_LOG_DIR}/qgis-server-access.log combined

  # Longer timeout for WPS... default = 40
  FcgidIOTimeout 300
  FcgidIdleTimeout 300
  FcgidProcessLifeTime 300
  FcgidInitialEnv LC_ALL "en_US.UTF-8"
  FcgidInitialEnv PYTHONIOENCODING UTF-8
  FcgidInitialEnv LANG "en_US.UTF-8"

  FcgidInitialEnv QGIS_DEBUG 1

  FcgidInitialEnv QGIS_DEBUG_FILE /var/log/qgis/cpp_server/qgis.log
  FcgidInitialEnv QGIS_LOG_FILE /var/log/qgis/cpp_server/qgis.log

  FcgidInitialEnv QGIS_SERVER_LOG_FILE /var/log/qgis/cpp_server/qgisserver.log
  FcgidInitialEnv QGIS_SERVER_LOG_LEVEL 0

  FcgidInitialEnv QGIS_PLUGINPATH "/opt/qgis-server/plugins"

  # Needed for QGIS HelloServer plugin HTTP BASIC auth
  #<IfModule mod_fcgid.c>
  #    RewriteEngine on
  #    RewriteCond %{HTTP:Authorization} .
  #    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
  #</IfModule>
  #
  #ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
  #<Directory "/usr/lib/cgi-bin">
  #    AllowOverride All
  #    Options +ExecCGI -MultiViews +FollowSymLinks
  #    # for apache2 > 2.4
  #    Require all granted
  #    #Allow from all
  #</Directory>

  # QGIS_AUTH_DB_DIR_PATH must lead to a directory writeable by the Servers FCGI process user
  FcgidInitialEnv QGIS_AUTH_DB_DIR_PATH ${HOME}"/qgisserverdb/"
  FcgidInitialEnv QGIS_AUTH_PASSWORD_FILE ${HOME}"/qgisserverdb/qgis-auth.db"

  # See http://docs.qgis.org/testing/en/docs/user_manual/working_with_vector/supported_data.html#pg-service-file
  SetEnv PGSERVICEFILE ${HOME}/.pg_service.conf
  FcgidInitialEnv PGPASSFILE ${HOME}"/.pgpass"

  # Tell QGIS Server instances to use a specific display number
  FcgidInitialEnv DISPLAY ":99"

  # if qgis-server is installed from packages in debian based distros this is usually /usr/lib/cgi-bin/
  # run "locate qgis_mapserv.fcgi"
  #  ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
  #  <Directory "/usr/lib/cgi-bin/">
  ScriptAlias / /usr/lib/cgi-bin/qgis_mapserv.fcgi
  <Directory "/usr/lib/cgi-bin/">
    RewriteEngine  on
    RewriteRule    "/testoo" "/"
    AllowOverride None
    Options +ExecCGI -MultiViews -SymLinksIfOwnerMatch
    Order allow,deny
    Allow from all
    Require all granted
  </Directory>

 <IfModule mod_fcgid.c>
    FcgidMaxRequestLen 26214400
    FcgidConnectTimeout 120
    FcgidIOTimeout 300
 </IfModule>

 </VirtualHost>' >> /tmp/001-qgis-server.conf
sudo mv /tmp/001-qgis-server.conf /etc/apache2/sites-available/001-qgis-server.conf -f

echo '<VirtualHost *:80>
  ServerAdmin webmaster@localhost
  DocumentRoot /var/www/html

  ErrorLog ${APACHE_LOG_DIR}/qgis-server-error.log
  CustomLog ${APACHE_LOG_DIR}/qgis-server-access.log combined

  # Longer timeout for WPS... default = 40
  FcgidIOTimeout 300
  FcgidIdleTimeout 300
  FcgidProcessLifeTime 300
  FcgidInitialEnv LC_ALL "en_US.UTF-8"
  FcgidInitialEnv PYTHONIOENCODING UTF-8
  FcgidInitialEnv LANG "en_US.UTF-8"

  FcgidInitialEnv QGIS_DEBUG 1

  FcgidInitialEnv QGIS_DEBUG_FILE /var/log/qgis/cpp_server/qgis.log
  FcgidInitialEnv QGIS_LOG_FILE /var/log/qgis/cpp_server/qgis.log

  FcgidInitialEnv QGIS_SERVER_LOG_FILE /var/log/qgis/cpp_server/qgisserver.log
  FcgidInitialEnv QGIS_SERVER_LOG_LEVEL 0

  FcgidInitialEnv QGIS_PLUGINPATH "/opt/qgis-server/plugins"

  # Needed for QGIS HelloServer plugin HTTP BASIC auth
  #<IfModule mod_fcgid.c>
  #    RewriteEngine on
  #    RewriteCond %{HTTP:Authorization} .
  #    RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
  #</IfModule>
  #
  #ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
  #<Directory "/usr/lib/cgi-bin">
  #    AllowOverride All
  #    Options +ExecCGI -MultiViews +FollowSymLinks
  #    # for apache2 > 2.4
  #    Require all granted
  #    #Allow from all
  #</Directory>

  # QGIS_AUTH_DB_DIR_PATH must lead to a directory writeable by the Servers FCGI process user
  FcgidInitialEnv QGIS_AUTH_DB_DIR_PATH ${HOME}"/qgisserverdb/"
  FcgidInitialEnv QGIS_AUTH_PASSWORD_FILE ${HOME}"/qgisserverdb/qgis-auth.db"

  # See http://docs.qgis.org/testing/en/docs/user_manual/working_with_vector/supported_data.html#pg-service-file
  SetEnv PGSERVICEFILE ${HOME}/.pg_service.conf
  FcgidInitialEnv PGPASSFILE ${HOME}"/.pgpass"

  # Tell QGIS Server instances to use a specific display number
  FcgidInitialEnv DISPLAY ":99"

  # if qgis-server is installed from packages in debian based distros this is usually /usr/lib/cgi-bin/
  # run "locate qgis_mapserv.fcgi"
  ScriptAlias /cgi-bin/ /usr/lib/cgi-bin/
  <Directory "/usr/lib/cgi-bin">
      AllowOverride All
      Options +ExecCGI -MultiViews +FollowSymLinks
      # for apache2 > 2.4
      Require all granted
      #Allow from all
  </Directory>

 <IfModule mod_fcgid.c>
    FcgidMaxRequestLen 26214400
    FcgidConnectTimeout 120
    FcgidIOTimeout 300
 </IfModule>

 </VirtualHost>' >> /tmp/002-qgis-server.conf
sudo mv /tmp/002-qgis-server.conf /etc/apache2/sites-available/002-qgis-server.conf -f


echo ------------------------------------------------------
echo        Droit sur Apache user : www-data
echo ------------------------------------------------------
sudo mkdir -p /var/log/qgis/cpp_server/cache/
sudo touch /var/log/qgis/cpp_server/qgisserver.log
sudo chown -R www-data:www-data /var/log/qgis/*
mkdir ${HOME}/qgisserverdb
sudo chown -R www-data:www-data ${HOME}/qgisserverdb


echo ------------------------------------------------------
echo        Enable FCGI daemon apach
echo ------------------------------------------------------
sudo a2enmod fcgid
sudo a2enmod rewrite
sudo a2ensite 001-qgis-server
sudo a2ensite 002-qgis-server


echo ------------------------------------------------------
echo        Copie dans dossier python plugins
echo ------------------------------------------------------
sudo apt install git
cd /tmp
rm -rf cpp_server/
git clone http://gitlab.com/collexpersee/cpp_server
cd /tmp/cpp_server
git checkout plugin
cp /tmp/cpp_server /usr/share/qgis/python/plugins/cpp_server/ -rf

#echo ------------------------------------------------------
#echo        Ressources pour plugin python
#echo ------------------------------------------------------
#sudo mkdir /usr/lib/cgi-bin/opentheso
#sudo chmod -R 777 /usr/lib/cgi-bin/opentheso/
#sudo cp /usr/lib/cgi-bin/qgis_mapserv.fcgi /usr/lib/cgi-bin/opentheso/ -f

echo ------------------------------------------------------
echo        Restart service Apache
echo ------------------------------------------------------
sudo service apache2 restart