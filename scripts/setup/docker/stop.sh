#!/usr/bin/env bash
docker stop $(docker ps -a | grep kartoza/qgis-server | awk '{print $1}')
docker stop $(docker ps -a | grep kartoza/postgis | awk '{print $1}')
docker stop $(docker ps -a | grep nikolaik/python-nodejs | awk '{print $1}')