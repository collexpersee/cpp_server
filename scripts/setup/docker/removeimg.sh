#!/usr/bin/env bash
#Remove all containers using the rabbitmq image:
docker rmi $(docker image ls -a | grep kartoza/qgis-server | awk '{print $3}')
docker rmi $(docker image ls -a | grep kartoza/postgis | awk '{print $3}')
docker rmi $(docker image ls -a | grep nikolaik/python-nodejs | awk '{print $3}')