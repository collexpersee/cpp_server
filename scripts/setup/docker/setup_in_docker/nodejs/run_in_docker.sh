#!/usr/bin/env bash
rm -rf /cpp_node
mkdir -p /cpp_node/cache/
cd /tmp
git clone http://gitlab.com/collexpersee/cpp_node
cp /tmp/cpp_node / -rf

cd /cpp_node
npm install
forever start -c "npm start" ./daemon.js
forever start -c "npm start" ./server.js

echo ------------------------------------------------------
echo        Running NodeJS
echo ------------------------------------------------------