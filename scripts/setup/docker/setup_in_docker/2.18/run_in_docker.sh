#!/usr/bin/env bash
#echo             UPDATE mise à jour des paquets
apt-get update  >/dev/null 2>&1
echo ------------------------------------------------------
echo                    INSTALL python-sparqlwrapper
echo ------------------------------------------------------
apt-get -y install python-sparqlwrapper  >/dev/null 2>&1
echo ------------------------------------------------------
echo                    INSTALL python tools
echo ------------------------------------------------------
apt-get -y install python-pytest  >/dev/null 2>&1
apt-get -y install flake8   >/dev/null 2>&1


echo -**********ONLY PYTHON2*******************************
apt-get -y install python-pip  >/dev/null 2>&1
echo -*****************************************************

pip install rdflib  >/dev/null 2>&1


echo ------------------------------------------------------
echo        Droit sur Apache user : www-data
echo ------------------------------------------------------
mkdir -p /var/log/qgis/cpp_server/cache/
touch /var/log/qgis/cpp_server/qgisserver.log
chown -R www-data:www-data /var/log/qgis/*
mkdir ${HOME}/qgisserverdb
chown -R www-data:www-data ${HOME}/qgisserverdb

#echo ------------------------------------------------------
#echo        Ressources pour plugin python
#echo ------------------------------------------------------
#mkdir /usr/lib/cgi-bin/opentheso
#chmod -R 777 /usr/lib/cgi-bin/opentheso/
#cp /usr/lib/cgi-bin/qgis_mapserv.fcgi /usr/lib/cgi-bin/opentheso/ -f

echo ------------------------------------------------------
echo        Plugin python
echo ------------------------------------------------------
apt install git  >/dev/null 2>&1
rm -rf /tmp/cpp_server/
cd /tmp
git clone http://gitlab.com/collexpersee/cpp_server
cd /tmp/cpp_server
git checkout plugin
cp /tmp/cpp_server /usr/share/qgis/python/plugins/cpp_server/ -rf
chown www-data:www-data /usr/share/qgis/python/plugins/cpp_server/*

echo ------------------------------------------------------
echo        Restart service Apache
echo ------------------------------------------------------
service apache2 reload

echo ------------------------------------------------------
echo        Running QGis Server 2.18
echo ------------------------------------------------------
