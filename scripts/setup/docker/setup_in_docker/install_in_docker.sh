#!/usr/bin/env bash


echo --------------------------------------------------------------------
echo DEPENDENCIES FOR QGIS SERVER 2.18
while [ "$(docker ps -f name=qgisserver2-container -f  status=running | grep qgisserver2-container)" = "" ]
do
    echo "...........waiting for qgisserver2-container.........."
    sleep 1
done
echo "........... qgisserver2-container - find .........."
docker exec qgisserver2-container /bin/sh /setup/2.18/run_in_docker.sh
echo "...........########## qgisserver2-container - OK ##########.........."
echo --------------------------------------------------------------------

echo --------------------------------------------------------------------
echo DEPENDENCIES FOR SERVER NODEJS
while [ "$(docker ps -f name=nodejs-python -f  status=running | grep nodejs-python)" = "" ]
do
    echo "...........waiting for nodejs-python.........."
    sleep 1
done
echo "........... nodejs-python - find .........."
docker exec nodejs-python /bin/sh /setup/nodejs/run_in_docker.sh
echo "...........########## nodejs-python - OK ##########.........."
echo --------------------------------------------------------------------
