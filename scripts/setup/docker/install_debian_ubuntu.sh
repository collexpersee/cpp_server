#!/usr/bin/env bash
## https://github.com/kartoza/docker-qgis-server
## https://docs.docker.com/install/linux/docker-ce/debian/

source globals.sh


echo ---------------------------------------------------------------------
echo INSTALLATION DOCKER
echo basé sur le git https://github.com/kartoza/docker-qgis-server
echo target : ${APPDIR}

# Uninstall old versions
echo "Uninstall old versions"
sudo apt remove docker docker-engine docker.io containerd runc  >/dev/null 2>&1

# Set up the utils dependencies
echo "Set up the dependencies for docker"
sudo apt update  >/dev/null 2>&1
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y  >/dev/null 2>&1

# on cherche OS : debian ou ubuntu
currentOS=$(cat /etc/issue|grep -Ei debian)
# on teste si on est sur ubuntu
if [ "$currentOS" = "" ]
then
    echo "OS : ubuntu"
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88  >/dev/null 2>&1
    sudo add-apt-repository \
        "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) \
        stable"  >/dev/null 2>&1
else
    echo "OS : debian"
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/debian \
       $(lsb_release -cs) \
       stable"  >/dev/null 2>&1
fi
echo ---------------------------------------------------------------------


echo ---------------------------------------------------------------------
echo INSTALLATION DOCKER-COMPOSE
# Install Docker CE
echo "Install Docker CE"
sudo apt-get update  >/dev/null 2>&1
sudo apt-get install docker-ce docker-ce-cli containerd.io -y  >/dev/null 2>&1
# Install docker compose
echo "Install docker compose"
sudo apt install docker-compose -y  >/dev/null 2>&1
echo ---------------------------------------------------------------------



echo ---------------------------------------------------------------------
echo DOCKER-COMPOSE UP
echo ===>Download dockerfile from kartoza
# prepare folders
echo "prepare folders"
sudo rm -rf ${APPDIR}
mkdir -p ${APPDIR}/${DOCKERTMP}/
cp -rf ./ ${APPDIR}/${DOCKERTMP}/
cd ${APPDIR}/${DOCKERTMP}/
#git clone in folder
echo "git ..."
sudo apt install git -y  >/dev/null 2>&1
git clone https://github.com/kartoza/docker-qgis-server  >/dev/null 2>&1
cd ${APPDIR}/${DOCKERTMP}/docker-qgis-server


echo ===> shared folder between containers
SHARED_FODLER=${HOME}/qgisserverdb/
SHARED_FODLER_DOCKER=/root/qgisserverdb/
mkdir -p ${SHARED_FODLER}


# modif yml dans le docker compose
rm docker-compose.yml
echo "
# Example QGIS Server docker compose
# This file will run two QGIS Server instances
# QGIS 2.18 LTR - on port 8080 publishing contents of /project
# QGIS 3.0 - on port 8081 publishing contents of /project-qgis3

# Please read the accompanying README for more details.

db:
  image: kartoza/postgis:9.6-2.4
  container_name: db-container
  #volumes:
  #- ./pg/postgres_data:/var/lib/postgresql
  environment:
    - USERNAME=docker
    - PASS=docker

qgisserver2:
  #build : 2.18
  image: kartoza/qgis-server:LTR
  hostname: qgis-server
  container_name: qgisserver2-container
  environment:
    # set to '' if you plan on hosting
    # multiple projects on this server an
    # then add map=<path to project> to your GET
    # requests
    - QGIS_PROJECT_FILE=/project/project.qgs
    # 0 = highest level, shows all messages
    # 5 = very quiet
    - QGIS_SERVER_LOG_LEVEL=0
  volumes:
    - ./project:/project
    - ../setup_in_docker:/setup
    - ${SHARED_FODLER}:${SHARED_FODLER_DOCKER}
  ports:
    - "8080:80"
  links:
    - db:db

qgisserver3:
  #build : 3,0
  image: kartoza/qgis-server:3.0.3
  hostname: qgis-server
  container_name: qgisserver3-container
  environment:
    # set to '' if you plan on hosting
    # multiple projects on this server an
    # then add map=<path to project> to your GET
    # requests
    - QGIS_PROJECT_FILE=/project/project.qgs
    # 0 = highest level, shows all messages
    # 5 = very quiet
    - QGIS_SERVER_LOG_LEVEL=0
  volumes:
    - ./project-qgis3:/project
    - ../setup_in_docker:/setup
    - ${SHARED_FODLER}:${SHARED_FODLER_DOCKER}
  ports:
    - "8081:80"
  links:
    - db:db

nodejs:
  image: nikolaik/python-nodejs:python3.7-nodejs11
  hostname: node-js
  container_name: nodejs-python
  volumes:
    - ../setup_in_docker:/setup
    - ${SHARED_FODLER}:/cpp_node/cache/geojson
  ports:
    - "3000:3000"
  tty: true
" >> docker-compose.yml

echo ${APPDIR}/${DOCKERTMP}
sh ${APPDIR}/${DOCKERTMP}/setup_in_docker/install_in_docker.sh&

docker-compose up
echo ============> Exit DockerApp