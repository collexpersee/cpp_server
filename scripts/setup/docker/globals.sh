#!/usr/bin/env bash
##
## GLOBALS
##
PORTQGIS2=8080
PORTQGIS3=8081
QGIS2SERVERNAME=qgisserver2-container
QGIS3SERVERNAME=qgisserver3-container
APPNAME=collexpersee
APPDIR=~/${APPNAME}
DOCKERTMP=docker
