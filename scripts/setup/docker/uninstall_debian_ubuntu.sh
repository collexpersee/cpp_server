#!/usr/bin/env bash
## https://github.com/kartoza/docker-qgis-server
## https://docs.docker.com/install/linux/docker-ce/debian/

source globals.sh

sudo sh stop.sh
sudo sh remove.sh
sudo sh removeimg.sh

echo ---------------------------------------------------------------------
echo UNINSTALLATION DOCKER
echo basé sur le git https://github.com/kartoza/docker-qgis-server
echo target : ${APPDIR}

# Uninstall old versions
sudo apt remove docker docker-engine docker.io containerd runc

# Set up the repository
sudo apt-get uninstall \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common


# Install Docker CE
sudo apt-get remove docker-ce docker-ce-cli containerd.io

# Install docker compose
sudo apt remove docker-compose

# download setup docker
sudo rm -rf ${APPDIR}
