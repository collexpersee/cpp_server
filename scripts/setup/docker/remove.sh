#!/usr/bin/env bash
#Remove all containers using the rabbitmq image:
docker rm $(docker ps -a | grep kartoza/qgis-server | awk '{print $1}')
docker rm $(docker ps -a | grep kartoza/postgis | awk '{print $1}')
docker rm $(docker ps -a | grep nikolaik/python-nodejs | awk '{print $1}')