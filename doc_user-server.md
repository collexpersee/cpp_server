# User guide

This is a user guide to give a quick overview of the cpp_server plugin server's installing. <br/>
Only for QGIS 2.18. <br/>
QGIS 3.0 is not yet operational. <br/>

## Requirement

To use this plugin server, you have to provide ubuntu 18.04 OS or debian 9. 

## Setup

Two installations are avaible : one installing the plugin with its dependencies on the qgis server plugin folder and another one with the docker technologies.

### Installation without Docker

#### installation

First, you should have QGIS Server 2.18 installed (can be on a virtual machine). <br/>
You can install QGis Server with this tutorial 

```url
https://docs.qgis.org/2.18/en/docs/training_manual/qgis_server/install.html
```

Can use on an OS or a Virtual Machine<br/>
Git the project and use the branch plugin or master <br/>
```bash
git clone http://gitlab.com/collexpersee/cpp_server
git checkout plugin
```
Run the script :

```bash
sh <download_location>/cpp_server/scripts/setup/vm/osgeo_qgis2.18/plugin_setup.sh
```
<br/>
The scripts installs all the necessaries packages including python, qt4 ...
<br/>
<br/>

#### Test the plugin

to test that the plugin is connected to qgis server, you can run the script :<br/>
```bash
sh <download_location>/cpp_server/scripts/tests/vm/serviceTest.sh
```
You must have the result:<br/>
```bash
Test CPP plugin!
```
to test that the plugin is functional, run the script:<br/>
```bash
sh <download_location>/cpp_server/scripts/tests/vm/serviceCppTestFilter.sh
```
You must have the result:<br/>
```json
[{"Persee_url": [], "Description": null, "long": "31.24798", "lat": "30.05169", "Bnf_url": [], "Name": "jardin de l'Ezbekieh"}]
```

### Installation with Docker

#### installation

**You have to use one of the require OS Ubuntu or Debian**

Can use on an OS or a Virtual Machine<br/>
Git the project and use the branch plugin or master <br/>
```bash
git clone http://gitlab.com/collexpersee/cpp_server
git checkout plugin
```
Run the script :
 ```bash
sh <download_location>/scripts/setup/docker/install_debian_ubuntu.sh
 ```
the scripts installs all the necessaries packages including docker. <br/>
It generates a docker compose file based on the kartoza docker to create the environnement.<br/>
```url
https://github.com/kartoza/docker-qgis-server
```
After installation, the script runs the environnement with these containers : qgis-server 2.18/3.0 and nodejs on docker containers.<br/>

#### Test the plugin

to test that the environnement is functional, you can run the script :<br/>
```bash
sh <download_location>/cpp_server/scripts/tests/docker/serviceTest.sh
```
You must have the result:<br/>
```bash
Test CPP plugin!
```
to test that the plugin is functional, run the script:<br/>
```bash
sh <download_location>/cpp_server/scripts/tests/docker/serviceCppTestFilter.sh
```
You must have the result:<br/>
```json
[{"Persee_url": [], "Description": null, "long": "31.24798", "lat": "30.05169", "Bnf_url": [], "Name": "jardin de l'Ezbekieh"}]
```


## How it works (for the 2 versions)
The service is avaible on the port : 
```url
http://localhost:8080/?param1=value1&param2=value2&...
http://localhost:80/cgi-bin/qgis_mapserv.fcgi?param1=value1&param2=value2&...
```
**Parameters** <br/>
The parameters can be : 

- SERVICE : the services avaible on plugin, can be one this list:
    - CPP : a service to check the plugin <br>
    - NODE : a service to communicate with the nodejs server <br>
    - WFS : QGis server services <br>
 
- SERVICE=CPP

    REQUEST= can be one this list :
    - TEST : to check if the plugin is connected to the server
    - FINDTEST : process a valid request
    - *FIND : the old process request using QGis Server* 

- SERVICE=NODE
    
    REQUEST=PUSH to create the thesaurus qgis project.<br>
    Here an example
    ```url
    http://locahost:8080/?SERVICE=NODE&REQUEST=PUSH&TH=opentheso&GAZETTEER=Modern Cairo Gazetteer
    ```
- SERVICE=WFS

    You can check API on QGis Server Tutotial :
    ```url
    https://docs.qgis.org/2.18/en/docs/user_manual/working_with_ogc/ogc_server_support.html
    ```
    