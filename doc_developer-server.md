# Comment faire un plugin server

## Choix des technologies

On a choisi une machine virtuelle préparée par la **Fondation Open Source Geospatial**
ayant déjà QGis Server configuré et installé. 

```url
https://www.osgeo.org/
https://fr.wikipedia.org/wiki/Fondation_Open_Source_Geospatial
```

### Machine Virtuelle

- VM Osgeo Live
- QGis 2.18 + Python 2.7

**QGIS comme server** <br>

Il faut configurer QGis comme server

```url
https://docs.qgis.org/2.14/fr/docs/user_manual/working_with_ogc/ogc_server_support.html
```

#### Installation

Il faut ajouter les paquets Qgis
```url
https://docs.qgis.org/2.18/fr/docs/training_manual/qgis_server/install.html
```

### Fonctionnement du Plugin QGis Server

![pluginQgisServer](images/pluginQgisServer_fonctionnement.png)

### Création Plugin QGis Server

*Liens utils :*

```url
https://docs.qgis.org/2.18/fr/docs/pyqgis_developer_cookbook/server.html#init-py
```

**Emplacement des plugins sous linux**

```url
/usr/share/qgis/python/plugins
```

**Architecture dossier**
```
PYTHON_PLUGINS_PATH/    --> *chemin des plugins python*
    Cpp/                --> *nom de notre plugin*
        __init__.py     --> *required*
        CppServer.py    --> *required* 
        metadata.txt    --> *required*
```

![umlServer](images/umlServer.png)


**Contenu fichier __init__.py**

```
# --> *obligatoire pour QGis*
def serverClassFactory(serverIface):
    from Cpp import CppServerServer
    return CppServerServer(serverIface)
```

**Contenu CppServer.py**

```
# xxxFilter classe à développer pour nos filtres dans url
class xxxFilter:
    
    def __init__(self, server_iface):
        super(xxxFilter, self).__init__(server_iface)

    def requestReady(self):
        pass

    def sendResponse(self):
        pass

    def responseComplete(self):
        pass
            
class CppServerServer:

    def __init__(self, server_iface):
        # Save reference to the QGIS server interface
        self.server_iface = server_iface
        priority = 1
        server_iface.registerFilter(xxxFilter(server_iface),
                                    100*priority)
        # ...
```
La fonction ***requestReady()*** est appelée lorsque la requête est prête. 
Elle permet et gérer l'URL entrant et ses paramètres avant de passer au travers les services du serveur.

La fonction ***sendResponse()*** est appelée lorsque la sortie est envoyée vers la sortie FCGI et  vers le client.
Elle est habituellement réalisée après la fin du traitement des services et après l'appel de la fonction responseComplete.
Mais, nous pouvons appeler la fonction sendReponse() avant la responseComplete() quand nous avons des services en flux.

La fonction ***responseComplete()*** est appelée lorsque les processus des services lancées se sont bien terminées et que la requête est prête pour être envoyée au client. 

### configuration apache2

La VM contient déjà les services apache HTTP, il faut cependant configurer apache pour QGisSever.

*Liens utils :*
```
https://docs.qgis.org/2.18/fr/docs/training_manual/qgis_server/install.html
http://www.itopen.it/qgis-server-python-plugins-ubuntu-setup/
```

**Dans la VM un projet QGis est déjà configuré pour le lancement du serveur.**

```
Paramètres additionnels supportés par tout type de requête¶

    Paramètres FILE_NAME: si configuré, la réponse du serveur sera renvoyée au client sous forme de fichier pièce-jointe avec le nom de fichier indiqué par le paramètre.

    Paramètre MAP: Comme avec MapServer, le paramètre MAP peut être utilisé pour spécifier le chemin vers le fichier projet QGIS. Vous pouvez indiquer un chemin absolu ou un chemin relatif à l’emplacement de l’exécutable du serveur (qgis_mapserv.fcgi). Si aucun chemin n’est indiqué, QGIS Server recherche les fichiers .qgs dans le dossier de son exécutable.

    Exemple:

    http://localhost/cgi-bin/qgis_mapserv.fcgi?\
      REQUEST=GetMap&MAP=/home/qgis/mymap.qgs&...

Note

Vous pouvez définir une variable d’environnement QGIS_PROJECT_FILE pour indiquer à l’exécutable du serveur où trouver le fichier de projet QGIS. Cette variable est l’emplacement à partir duquel QGIS lira le fichier de projet. Si cette variable n’est pas définie, le serveur utilisera le paramètre MAP de la requête et, en dernier recours, cherchera dans le répertoire de l’exécutable du serveur.
Pour la communication, nous utilisons le module fastCGI (mod_fcgid) de Apache.

https://docs.qgis.org/2.14/fr/docs/user_manual/working_with_ogc/ogc_server_support.html#extra-parameters-supported-by-the-wfs-getfeature-request
https://docs.qgis.org/2.18/fr/docs/training_manual/qgis_server/install.html

```


## QGIS API C++

- **QgsServerInterface**

Elle définit les interfaces exposées par QGIS Server et mises à la disposition des plugins.

- **QgsServerFilter**

Ça permet de modifier le paramètres d'entrée, modifier la sortie ou encore fournir de nouveaux services.
Elle enregistre un ou plusieurs filtres. Chaque filtre contient au moins un des trois fonctions de rappel(requestReady(), reponseComplete(), sendResponse()).

- **QgsAccessControlFilter**

Avec laquelle nous pouvons appliquer des restrictions d'accés sur chaque requête.