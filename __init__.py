# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


# # https://docs.python.org/3/howto/logging.html
# from filters.modules.commons.util import *
# logging.basicConfig(filename='dbg.log', filemode='a',
#                     format='%(asctime)s - %(levelname)s - %(message)s', level=g_logger.info,
#                     datefmt='%m/%d/%Y %I:%M:%S %p')


def serverClassFactory(serverIface):
    """
    Fonction obligatoire pour utiliser le plugin avec QGis Server
    C'est le point d'entrée
    :param serverIface: QgsServerInterface renseigné par le server
    :return: CppServerServer La classe développée qui prend comme argument de construction "serverIface"
            CppServerServer doit renseigner les filtres pour l'url : http://127.0.0.1/SERVICE=XXXX & REQUEST=YYYY ...
    """
    from CppServer import CppServerServer
    return CppServerServer(serverIface)
