# Collex persée - Displaying archaeological data in Cairo city

Collex persée is an application for historian searchers which allow them to request geolocated data from theosaurus and semantic web database. 
These data can be historical data such as photos and writtings collected by archaeologists. They are display on a **QGIS** client to be analysed.

## How does it work ##

There are three components : 
- A server plugin, running a Qgis server (back-end) that is presented on this page
- A client plugin, running a Qgis (front-end) which can be find in gitlab [here](https://gitlab.com/collexpersee/cpp_client)
- A server web, running a node server which can be find in gitlab [here](https://gitlab.com/collexpersee/cpp_node)


Depending your configuration, you can run server plugin in localhost, or through web.


## How to use 

Please refer to the next two documentation for further informations :

[Developers Documentation](https://gitlab.com/collexpersee/cpp_server/blob/master/doc_developer-server.md)

[Users Documentation](https://gitlab.com/collexpersee/cpp_server/blob/master/doc_user-server.md)


## Report

*logins : the same login and password of gmail - already given by email*

The folder */documents* contains our reports :

- CR_CollexPersee.zip the latex archive file of the project report, this report can be accessed in overleaf [here](https://www.overleaf.com/project/5c8a300dd10f18477e81fb37?ws=fallback), with the *logins*
- ppt_collexpersee.pttx the project presentation
- .mp4 the videos demos, these videos can be accessed on youtube with the *logins* [here](https://www.youtube.com/channel/UCHgJyuQ1YjZU6nCCh_zWjvQ)

Diagrams (and history) of the projects are accessible in the wiki [here](https://gitlab.com/collexpersee/cpp_server/wikis/home)


> Authors : A. Fondere, C. Crapart, E. Viegas, M. Permalnaick, S. Thaalbi, T. Harling, W. Podlejski