# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# from __future__ import absolute_import

__author__ = 'ENSG TSI'
__date__ = 'Avril 2019'
__copyright__ = '(C) 2018, ENSG '

from filters.TestFilter import TestFilter
from filters.CppTestFilter import CppTestFilter, fill_request_test
from filters.CppFilter import CppFilter
from filters.ExceptionFilter import ExceptionFilter
from filters.modules.commons.util import g_logger
from filters.NodeCppFilter import NodeCppFilter


# if hasattr('g_logger', 'init'):
g_logger.init()


class CppServerServer:
    """
    Classe filtre qui hérite de QgsServerFilter pour la manipulation des filtres URL
    """

    def __init__(self, server_iface):
        """
        :param server_iface: QgsServerInterface renseigné par le server
        """
        # Save reference to the QGIS server interface
        self.server_iface = server_iface
        priority = 1
        server_iface.registerFilter(TestFilter(server_iface),
                                    100 * priority)
        priority += 1
        server_iface.registerFilter(CppTestFilter(server_iface),
                                    100 * priority)
        priority += 1
        server_iface.registerFilter(CppFilter(server_iface),
                                    100 * priority)
        priority += 1
        server_iface.registerFilter(NodeCppFilter(server_iface),
                                    100 * priority)
        priority += 1
        server_iface.registerFilter(ExceptionFilter(server_iface),
                                    100 * priority)


if __name__ == '__main__':
    # import postgresql
    # db = postgresql.open('pq://user:password@host:port/database')
    # db.execute("CREATE TABLE emp (emp_first_name text, emp_last_name text, emp_salary numeric)")
    # make_emp = db.prepare("INSERT INTO emp VALUES ($1, $2, $3)")
    # make_emp("John", "Doe", "75,322")
    # with db.xact():
    #     make_emp("Jane", "Doe", "75,322")
    # make_emp("Edward", "Johnson", "82,744")

    # print(QgsLogger.mro())
    from qgis.core import QgsLogger
    g_logger.init()
    print(QgsLogger.debugLevel())
    print(QgsLogger.logFile())
    g_logger.info("g_logger.info test")

    t_result_json = fill_request_test()
    print(t_result_json)
