# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from qgis.server import QgsServerFilter
from qgis.server import QgsRequestHandler
from .modules.commons.util import g_logger, require
from .modules.commons.request import Request
from .modules.facaderequest import FacadeRequest


class CppFilter(QgsServerFilter):
    """
    Classe filtre qui hérite de QgsServerFilter pour la manipulation des filtres URL
    :exemples:
        wget -q -O - "http://localhost:81/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=PLACE"
        wget -q -O - "http://localhost:81/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=PLACETEST"
    """

    def __init__(self, server_iface):
        """
        :param server_iface: QgsServerInterface renseigné par le server
        """
        super(CppFilter, self).__init__(server_iface)
        g_logger.debug("CppFilter.__init__")

    def requestReady(self):
        """
        La fonction requestReady() est appelée lorsque la requête est prête.
        Elle permet et gérer l'URL entrant et ses paramètres avant de passer au travers les services du serveur.
        """
        g_logger.info("CppFilter.requestReady")

    def sendResponse(self):
        """
        La fonction sendResponse() est appelée lorsque la sortie est envoyée vers la sortie FCGI et  vers le client.
        Elle est habituellement réaligit sée après la fin du traitement des services et après l'appel de la fonction
        responseComplete. Mais, nous pouvons appeler la fonction sendReponse() avant la responseComplete()
        quand nous avons des services en flux.
        """
        g_logger.info("CppFilter.sendResponse")

    def responseComplete(self):
        """
        La fonction responseComplete() est appelée lorsque les processus des services lancées se sont bien terminées
        et que la requête est prête pour être envoyée au client.
        """
        g_logger.info("CppFilter.responseComplete")
        request_handler = self.serverInterface().requestHandler()

        try:
            if request_handler.parameter('SERVICE').upper() == 'CPP'\
                    and request_handler.parameter('REQUEST').upper() == 'FIND':
                request_handler.clearHeaders()
                request_handler.setHeader('Content-type', 'text/plain')
                request_handler.clearBody()

                my_result_json = self.fill_request(request_handler)
                request_handler.appendBody(my_result_json)

        except Exception as e:
            tmp = 'Exception capturée :' + str(e)
            g_logger.info(tmp)
            request_handler.appendBody(tmp)
            request_handler.appendBody("\n")

    #
    # refactoring function
    @require('request_handler', QgsRequestHandler)
    def fill_request(self, request_handler):
        """
        refactoring method
        """
        import urllib
        my_request = Request()
        my_request.thesaurus = urllib.unquote(request_handler.parameter('TH')).decode('utf8')
        my_request.thesaurus_url = urllib.unquote(request_handler.parameter('URL')).decode('utf8')
        my_request.gazetteer = urllib.unquote(request_handler.parameter('GAZETTEER')).decode('utf8')
        my_request.keyword = urllib.unquote(request_handler.parameter('KEYWORD')).decode('utf8')

        my_request.wkt = urllib.unquote(request_handler.parameter('WKT')).decode('utf8')
        my_request.directory = urllib.unquote(request_handler.parameter('DIR')).decode('utf8')
        tmplng = request_handler.parameter('LONG')
        tmplat = request_handler.parameter('LAT')
        tmprad = request_handler.parameter('RD')
        if tmplng != '':
            my_request.longitude = float(tmplng)
        if tmplat != '':
            my_request.latitude = float(tmplat)
        if tmprad != '':
            my_request.radius = float(tmprad)
        my_request.validate()

        my_requestface = FacadeRequest(my_request)
        my_result_json = my_requestface.get_result()

        g_logger.debug('my_request=' + my_request.__str__())
        g_logger.debug('my_result_json' + my_result_json)

        return my_result_json
