# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

# __all__ = []
#
# import os
# import glob
# import sys
# import pkgutil
# import inspect
#
# modules = glob.glob(os.path.dirname(__file__)+"/module_*.py")
# local_modules = [os.path.basename(f)[+7:-3] for f in modules]

# for loader, name, is_pkg in pkgutil.walk_packages(__path__):
#     module = loader.find_module(name).load_module(name)
#
#     for namemodule, value in inspect.getmembers(module):
#         if not namemodule in local_modules:
#             continue
#         globals()[namemodule] = value
#         __all__.append(namemodule)

#
# if __name__ == '__main__':
#     print local_modules
#     path = os.path.join(os.path.dirname(__file__))
#     lists = pkgutil.walk_packages(path)
#     for loader, name, is_pkg in lists :
#         module = loader.find_module(name).load_module(name)
#
#         for namemodule, value in inspect.getmembers(module):
#             if not namemodule in local_modules:
#                 continue
#             globals()[namemodule] = value
#             __all__.append(namemodule)
#     for filter_name in local_modules:
#         try:
#             print " a "
#
#             r = getattr("module_"+filter_name, filter_name)()
#
#         except Exception as e:
#             print e
