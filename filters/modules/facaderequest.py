# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from .commons.util import require, g_logger
from .commons.request import Request
from .commons.factorythesaurus import FactoryThesaurus


class FacadeRequest:
    """
    Facade pour les Requetes, créée pour faciliter l'utilisation
    Opérateur doit juste lancer get_result() pour obtenir le JSON
    """
    @require('request', Request)
    def __init__(self, request):
        g_logger.info('FacadeRequest.__init__:request ' + request.__str__())
        self.request = request

    def get_result(self):
        """
        lance le traitement de la requête et retourne un JSON en cas d'erreur Exception
        :return: JSON
        """
        g_logger.info('FacadeRequest.get_result')

        myfactory = FactoryThesaurus()
        mygazeteer = myfactory.create_thesaurus(self.request)
        mygazeteer.connexion(self.request)
        myresult = mygazeteer.conversion()
        return myresult.send_as_json()

    def __str__(self):
        return self.request.__str__()
