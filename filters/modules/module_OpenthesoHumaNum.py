# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
import os
import requests
from .commons.util import require, g_logger, working_path_cache, project_path
from .commons.interfacethesaurus import InterfaceThesaurus
from .commons.result import Result
from .commons.request import Request
from .commons.parser_rdf import ParserRdf
from datetime import datetime, timedelta


class OpenthesoHumaNum(InterfaceThesaurus):
    def __init__(self):
        g_logger.info("OpenthesoHumaNum.__init__")
        self.places = set()
        self.request_orig = Request()
        self.has_coordinates = False

        self.has_persee_link = False
        self.persee_data = ''

        self.has_bnf_link = False
        self.bnf_data = ''
        self.fullpathname = project_path() + 'tests/unit/resources/rdf_test.xml'
        self.my_parser = None

    @require("request", Request)
    def _parse_last_database_opentheso(self, fullpathname, request):
        """
        IT IS USEFUL TO USE urllib2 TO Download last version
        Here a saved version is loaded in cache folder
        :return: Parser object
        """
        g_logger.info("OpenthesoHumaNum._parse_last_database_opentheso")

        if request.thesaurus_url is not None:
            # decode uri
            idg_str = request.gazetteer.split('&')[0]
            th_str = request.gazetteer.split('&')[1]
            idg = idg_str.split('=')[1]
            th = th_str.split('=')[1]
            namefile = "idg=" + idg + "&th=" + th + ".xml"
            fullpathname = working_path_cache() + namefile
            g_logger.debug("OpenthesoHumaNum._parse_last_database_opentheso.fullpathname=" + fullpathname)

            # compare recent file with the old one : True means similar and False means different
            # https://stackoverflow.com/questions/237079/how-to-get-file-creation-modification-date-times-in-python
            # print(filecmp.cmp(fullpathname, '/var/log/qgis/cpp_server/cache/idg=MT62&th=42.xml'))
            b_download_file = self._check_file(fullpathname)

            if b_download_file:
                # download gazettier.xml
                self._download_database(fullpathname, request, idg, th)
        else:
            g_logger.warning("OpenthesoHumaNum._parse_last_database_opentheso.default file=" + fullpathname)

        # parse rdf file
        g_logger.debug('OpenthesoHumaNum._parse_last_database_opentheso.parse rdf file ' + fullpathname)
        parser = ParserRdf(fullpathname)
        return parser

    def _download_database(self, fullpathname, request, idg, th):
        g_logger.info('OpenthesoHumaNum._download_database.Beginning file download with requests')
        str_gazettier = "idg=" + idg + "&th=" + th
        url_concat = request.thesaurus_url + '/' + str_gazettier
        g_logger.debug('url=' + url_concat)
        rq = requests.get(url_concat)
        g_logger.debug("OpenthesoHumaNum._download_database.rq.text=" + rq.content)
        # send a HTTP request to the server and save
        # the HTTP response in a response object called rq
        with open(fullpathname, 'wb') as f:
            f.write(rq.content)
        pass
        g_logger.debug('OpenthesoHumaNum._download_database.Ecriture dans fichier réussi')

    def _check_file(self, fullpathname):
        b_download_file = False
        if os.path.isfile(fullpathname):
            current_time = datetime.now()
            t = os.path.getmtime(fullpathname)
            date_file = datetime.fromtimestamp(t)
            delta = current_time - timedelta(days=7)
            if delta > date_file:
                g_logger.debug("OpenthesoHumaNum.connection.fichier obselete")
                b_download_file = True
                pass
            else:
                g_logger.debug("OpenthesoHumaNum.connection.fichier ok")
                pass
        else:
            g_logger.debug("OpenthesoHumaNum.connection.fichier non existant doit telecharger")
            b_download_file = True
            pass
        return b_download_file

    @require("request", Request)
    def connexion(self, request):
        """
        thesaurus_url On doit utiliser le this.gazetteer pour le téléchargement du bazar
        UNFINISHED
        Request using Parser class with specific search:
        By keyword or by place
        note : place can be only lat long presently
        TO DO :
        - search by keyword with location restrain (will be adde in parser_rdf.py)
        - search by location using geometry
        - search by location using name location
        :param request: request object with parameters for searching
        :return:
        """
        g_logger.info("OpenthesoHumaNum.connexion:connect with request:" + request.__str__())

        self.request_orig = request
        self.my_parser = self._parse_last_database_opentheso(self.fullpathname, self.request_orig)

        if self.request_orig.is_blank():
            g_logger.debug("here we have to search and retrieve all documents")
            # here we have to search and retrieve all documents
            self.places = self.my_parser.get_all()
        else:
            g_logger.debug("rdf request by directory or latitude ... ")
            try:
                self.places = self.my_parser.request(self.request_orig.directory, self.request_orig.keyword,
                                                     self.request_orig.latitude, self.request_orig.longitude,
                                                     self.request_orig.radius)
                pass
            except Exception as e:
                g_logger.error("my_parser.request " + e)
                pass
        g_logger.debug("OpenthesoHumaNum.connexion:requête terminée : ")

    def conversion(self):
        """
        Takes a list in self.raw_result and build attributes of Result class
        :return: Result object
        """
        g_logger.info("OpenthesoHumaNum.conversion")

        result = Result()
        for place in self.places:
            result.listName.append(place.name)
            result.listLat.append(place.lat)
            result.listLong.append(place.long)
            result.listPersee.append(place.persee_links)
            result.listBnf.append(place.bnf_links)
            result.listDesc.append(place.description)
        return result

    def add_request_persee(self):
        g_logger.info('OpenthesoHumaNum.add_request_persee')

        if self.has_persee_link:
            # TO DO
            # implement here
            pass
        pass

    def __str__(self):
        return self.places.__str__()


if __name__ == '__main__':
    print('Subclass:', issubclass(OpenthesoHumaNum, InterfaceThesaurus))
    print('Instance:', isinstance(OpenthesoHumaNum(), InterfaceThesaurus))
