# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from .util import require, g_logger
from .request import Request
from ..module_OpenthesoHumaNum import OpenthesoHumaNum


class FactoryThesaurus:
    def __init__(self):
        self.request = None
        self.thesaurus = None

    @require("request", Request)
    def create_thesaurus(self, request):
        g_logger.info("FactoryThesaurus.create_thesaurus")
        self.request = request

        # import src
        # for filter_name in src.local_modules:
        #     g_logger.info("HelloServerServer
        #     - loading filter %s" % filter_name)
        # import commons
        # for filter_name in commons.local_modules:
        #     g_logger.info("HelloServerServer - loading filter %s" % filter_name)
        #     try:
        #         getattr(commons, filter_name)()
        #
        #     except Exception as e:
        #         g_logger.info("HelloServerServer
        #         - Error loading filter %s : %s" % (filter_name, e))

        if self.request.thesaurus == "OpenthesoHumaNum":
            self.thesaurus = OpenthesoHumaNum()
        else:
            g_logger.error("Erreur de type de thesaurus")
            raise ValueError("Erreur de type de thesaurus")

        return self.thesaurus
