# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import os
import logging
import logging.handlers
from qgis.core import QgsLogger


# http://code.activestate.com/recipes/454322-type-checking-decorator/
# pour forcer les types des arguments
def require(arg_name, *allowed_types):
    def make_wrapper(f):
        if hasattr(f, "wrapped_args"):
            wrapped_args = getattr(f, "wrapped_args")
        else:
            code = f.func_code
            wrapped_args = list(code.co_varnames[:code.co_argcount])
        try:
            arg_index = wrapped_args.index(arg_name)
        except ValueError:
            raise NameError(arg_name)

        def wrapper(*args, **kwargs):
            if len(args) > arg_index:
                arg = args[arg_index]
                if not isinstance(arg, allowed_types):
                    type_list = " or ".join(str(allowed_type)
                                            for allowed_type in allowed_types)
                    raise "Expected '%s' to be %s; was %s." \
                          % (arg_name, type_list, type(arg)), TypeError
            else:
                if arg_name in kwargs:
                    arg = kwargs[arg_name]
                    if not isinstance(arg, allowed_types):
                        type_list = " or " \
                            .join(str(allowed_type)
                                  for allowed_type in allowed_types)
                        raise "Expected '%s' to be %s; was %s." \
                              % (arg_name, type_list, type(arg)), TypeError

            return f(*args, **kwargs)

        wrapper.wrapped_args = wrapped_args
        return wrapper

    return make_wrapper


#
# pour obtenir le répertoire du projet
#
def project_path():
    path = os.path.dirname(__file__) + "/../../../"
    return os.path.abspath(path) + "/"


#
# pour obtenir le répertoire cache du projet
#
def working_path_cache():
    return "/var/log/qgis/cpp_server/cache/"


#
# pour obtenir le répertoire de travail du projet
#
def working_path():
    return "/var/log/qgis/cpp_server/"


#
# Debug
#
class CppLogging:
    """
    pour le debug et le logging
    """
    def __init__(self):
        self.instance = None
        self.fh = None
        self.formatter = None
        self.filename = ''
        self.qgsLogger = QgsLogger()

    def init(self, level=logging.DEBUG, filename=None):
        """
        :param level: logging.DEBUG, logging.INFO, logging.WARNING, logging.ERROR
        :param filename:
        :return:
        """
        try:
            if filename is None:
                self.filename = working_path() + 'qgisserver.log'
            else:
                self.filename = filename
            self.fh = logging.handlers.RotatingFileHandler(self.filename, maxBytes=10000000, backupCount=10)
            self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            self.fh.setFormatter(self.formatter)
            self.instance = logging.getLogger()
            self.instance.addHandler(self.fh)
            self.instance.setLevel(level)
            self.instance.debug('\n\n\n ----- START SESSION ----- \n\n')
        except Exception as e:
            print(e)
            pass
        pass

    def is_init(self):
        if self.instance is None:
            return False
        else:
            return True

    def debug(self, message):
        self.qgsLogger.debug(message)
        if self.instance is not None:
            self.instance.debug(message)
        pass

    def info(self, message):
        self.qgsLogger.debug(message)
        if self.instance is not None:
            self.instance.info(message)
        pass

    def error(self, message):
        self.qgsLogger.critical(message)
        if self.instance is not None:
            self.instance.error(message)
        pass

    def warning(self, message):
        self.qgsLogger.warning(message)
        if self.instance is not None:
            self.instance.warning(message)
        pass


#
# variable globale pour logs
#
# from qgis.core import QgsLogger as g_logg
g_logger = CppLogging()
