# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""

from __future__ import absolute_import
# import string
import os
# import sys
# import subprocess
from qgis.core import QgsProject, QgsApplication, QgsVectorLayer, QgsMapLayerRegistry
# from qgis.gui import *
from PyQt4.QtCore import QFileInfo
# from PyQt4.QtGui import QApplication
# from PyQt4.QtXml import *


def geojson_to_proj_qgs(theso, gazeteer, path_gjs):
    """
    It is better if yuo already executed script/setupopentheso.sh.
    This function create .qgs projets. After it, Qgis server will ask
    and retrieve information at each WFS connection on these .qgs projects.
    The source file is a geojson stored in the path :path variable
    The storage is created dynamically, in a folder named following variable theso.
    The name of qgs project will be the same.
    And each layer will be named following variable gazeteer.
    :param theso: the name of thesaurus
    :param gazeteer: the name of gazeteer
    :param path_gjs: the source path where all geojson are written

    !!!!!
    AT THE MOMENT ONLY ONE GAZETEER CAN BE STORED PER .QGS PROJECT.

    """
    # make sur all owners can modifiy this folder
    # subprocess.call(['sudo', 'chmod', '-R', '777', '/usr/lib/cgi-bin/'])
    # str_projet_name = '/usr/lib/cgi-bin/{}/{}.qgs'.format(theso, theso)
    str_projet_name = os.getenv("HOME")+'/qgisserverdb/{}/{}.qgs'.format(theso, theso)
    # create the folder with theso name
    # if not os.path.exists('/usr/lib/cgi-bin/{}'.format(theso)):
    if not os.path.exists(os.getenv("HOME")+'/qgisserverdb/{}'.format(theso)):
        os.mkdir(os.getenv("HOME")+'/qgisserverdb/{}'.format(theso))
        # subprocess.call(['sudo', 'chmod', '-R', '777', '/usr/lib/cgi-bin/{}'.format(theso)])
    # copy a cgi file from the /usr/lib/cgi-bin
    # if not os.path.isfile('/usr/lib/cgi-bin/{}/' \
    # 'qgis_mapserv.fcgi'.format(theso)):
    # subprocess.call(['cp', '/usr/lib/cgi-bin/qgis_mapserv.fcgi',
    # '/usr/lib/cgi-bin/{}/'.format(theso)])
    # remove old qgis project
    # if os.path.isfile(strProjetName):
    #    os.remove(strProjetName)
    # QgsApplication.setPrefixPath('/home/user/.qgis2', True)
    # QGISAPP = QgsApplication(sys.argv, True)
    QgsApplication.initQgis()
    # where is located the project
    QgsProject.instance().setFileName(str_projet_name)
    # create a layer to represent the geojson of a given gazeteer
    layer = QgsVectorLayer(path_gjs, gazeteer, 'ogr')
    # Add layer to the registry
    QgsMapLayerRegistry.instance().addMapLayer(layer)
    gazeteer_l = []
    gazeteer_l.append(layer.id())
    # create capabilities to enable WFS request on it
    QgsProject.instance().writeEntry('WFSLayers', "/", gazeteer_l)
    QgsProject.instance().writeEntry('WMSServiceCapabilities', "/", "True")
    QgsProject.instance().write(QFileInfo(str_projet_name))
    QgsApplication.exitQgis()
    return layer.isValid()
