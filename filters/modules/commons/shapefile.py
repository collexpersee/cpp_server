# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from .util import require
from .result import Result


class Shapefile:
    # DEPRECIED, See in Client plugin
    def __init__(self):
        self.place_name = ''
        self.object_name = ''
        self.picture_url = ''
        # coordinates is a string in Shapefile
        self.coordinates = ''
        self.theme = ''
        self.persee_results = ''
        self.author_name = ''
        self.date = ''
        self.abstract_words = ''
        self.result = Result()

    @require('result', Result)
    def create_shp(self, result):
        # generate result object
        self.result = result
        # self.convert_coord()
        # TO DO
        # implement Shapefile object
        pass

    # convert coordinate to string
    def convert_coord(self):
        # self.coordinates = str(self.result.coordinate[0])\
        #     + ',' + str(self.result.coordinate[1])
        return self.coordinates
