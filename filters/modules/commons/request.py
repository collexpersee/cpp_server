# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""


class Request:
    def __init__(self):
        # default value
        self.thesaurus = ''
        self.thesaurus_url = ''
        self.gazetteer = ''
        self.keyword = None
        self.longitude = None
        self.latitude = None
        self.radius = None
        self.directory = None
        self.wkt = ''

    def is_blank(self):
        if self.keyword is None and self.directory is None \
                and (self.latitude is None or self.longitude is None or self.radius is None):
            return True
        return False

    def validate(self):
        """
        pour wiltold : rdf_parser
        """
        if self.keyword == '':
            self.keyword = None
        if self.longitude == '':
            self.longitude = None
        if self.latitude == '':
            self.latitude = None
        if self.radius == '':
            self.radius = None
        if self.directory == '':
            self.directory = None
        pass

    def __str__(self):
        stmp = '{'
        stmp += 'thesaurus='
        if self.thesaurus is not None:
            stmp += self.thesaurus + ', '
        stmp += 'thesaurus_url='
        if self.thesaurus_url is not None:
            stmp += self.thesaurus_url + ', '
        stmp += 'keyword='
        if self.keyword is not None:
            stmp += self.keyword + ', '
        stmp += 'gazetteer='
        if self.gazetteer is not None:
            stmp += self.gazetteer + ', '
        stmp += 'longitude='
        if self.longitude is not None:
            stmp += self.longitude.__str__() + ', '
        stmp += 'latitude='
        if self.latitude is not None:
            stmp += self.latitude.__str__() + ', '
        stmp += 'radius='
        if self.radius is not None:
            stmp += self.radius.__str__() + ', '
        stmp += 'directory='
        if self.directory is not None:
            stmp += self.directory
        stmp += '}'
        return stmp
