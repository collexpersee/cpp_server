# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import json


class Result:
    def __init__(self):
        self.listName = []
        self.listLat = []
        self.listLong = []
        self.listPersee = []
        self.listBnf = []
        self.listDesc = []
        self.AllAtrib = ['Name', 'lat', 'long', 'Persee_url', 'Bnf_url', 'Description']
        self.json = ''

    def send_as_json(self):
        """
        Convert a dic into json using json library
        :return: a Json string with this format:
        [{"Name": "Place_bonbon", "lat": "4.5", "long": "6.5",
        "Persee_url": "http://", "Bnf_url": "http://", "Description": "bar"},
        {"Name": "Place_chocolat", "lat": "2.4", "long": "40.0",
        "Persee_url": "http://", "Bnf_url": "http://", "Description": "foo"}]
        """
        send_json = json.dumps([{self.AllAtrib[0]: self.listName[e],
                                 self.AllAtrib[1]: self.listLat[e],
                                 self.AllAtrib[2]: self.listLong[e],
                                 self.AllAtrib[3]: self.listPersee[e],
                                 self.AllAtrib[4]: self.listBnf[e],
                                 self.AllAtrib[5]: self.listDesc[e]}
                                for e in range(len(self.listName))])
        self.json = send_json
        return send_json

    def __str__(self):
        return self.json
