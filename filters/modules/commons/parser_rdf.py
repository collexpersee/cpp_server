# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from .place import Place
from .util import g_logger
import rdflib
import time
import re
import math


class ParserRdf:
    def __init__(self, source):
        """
        :param source: the RDF source, can be a file path or a string
        """
        g_logger.info('ParserRdf')
        self.graph = rdflib.Graph()
        g_logger.info('ParserRdf.parse.source')
        self.graph.parse(source)
        self.root = self.get_first_node()

    def get_first_node(self):
        """
            :return: the name of the root node of the theosaurus (gazetteer)
        """
        request = self.graph.query(
            """
            select distinct ?label
            where {
                ?id skos:prefLabel ?label.
                filter not exists { ?id skos:inScheme ?c}
                filter (lang(?label) = "fr")
            }"""
        )
        for row in request:
            return row[0]
        return ''

    def get_directories(self):
        """
            :return: the entire node tree with all directories
        """
        request = self.graph.query(
            """
            select distinct ?label ?bLabel
            where {
                ?id skos:prefLabel ?label.
                ?id skos:narrower ?b .
                ?b skos:prefLabel ?bLabel.
                filter exists { ?b skos:narrower ?d}.
                filter ( lang(?label) = "fr").
                filter ( lang(?bLabel) = "fr").
            }"""
        )
        dico = {}
        for row in request:
            if row[0] == self.root:
                parent = "root"
            else:
                parent = str(row[0])
            child = str(row[1])

            if parent not in dico:
                dico[parent] = [child]
            else:
                dico[parent].append(child)
            if child not in dico:
                dico[child] = []

        return dico

    def get_from_directory(self, directory):
        """
        :param directory: the place category to search
        :return: the place list of this category
        """
        if directory == "root":
            return self.get_all()
        request = self.graph.query(
            """
            select distinct ?label ?lat ?long ?bnf ?persee ?description
            where {
                ?id skos:prefLabel ?label.
                ?id geo:lat ?lat.
                ?id geo:long ?long.
                ?id skos:broader+ ?b .
                optional {?id skos:note ?bnf}
                optional {?id skos:editorialNote ?persee}
                optional {?id skos:definition ?description}
                filter exists { ?b skos:prefLabel """ +
            '"' + directory + '"' + """@fr}
                filter not exists {?id skos:narrower ?c}
                filter ( lang(?label) = "fr")
            }"""
        )
        return self.request_to_places(request)

    def get_all(self):
        """
            :return: the place list of the entire content of the gazetteer
        """
        request = self.graph.query(
            """
            select distinct ?label ?lat ?long ?bnf ?persee ?description
            where {
                ?id skos:prefLabel ?label.
                ?id geo:lat ?lat.
                ?id geo:long ?long.
                ?id skos:broader+ ?b .
                optional {?id skos:note ?bnf}
                optional {?id skos:editorialNote ?persee}
                optional {?id skos:definition ?description}
                filter not exists {?id skos:narrower ?c}
                filter ( lang(?label) = "fr")
            }"""
        )
        return self.request_to_places(request)

    def get_from_location(self, lat, long, radius):
        """
        Query for the place within the radius distance
        of the location of (latitude, longitude). There is
        an approximation of 0.5% on the compute of the distance.

        :param lat: the latitude of the starting location
        :param long: the longitude of the starting location
        :param radius: the radius of the buffer in kilometres
        :return the result of the request
        """
        a = 111.1
        b = a * math.cos(lat * math.pi / 180)
        a_carre = a**2
        b_carre = b**2
        radius_carre = radius**2
        request = self.graph.query(
            """
            select distinct ?label ?lat ?long ?bnf ?persee ?description
            where {
                ?id skos:prefLabel ?label.
                ?id geo:lat ?lat.
                ?id geo:long ?long.
                ?id skos:broader+ ?b.
                filter( ((?lat - """ + str(lat) + """) * (?lat - """ +
            str(lat) + """)) * """ + str(a_carre) + """ + ((?long - """ +
            str(long) + """) * (?long - """ + str(long) + """)) * """ +
            str(b_carre) + """ < """ + str(radius_carre) + """).
                optional {?id skos:note ?bnf}
                optional {?id skos:editorialNote ?persee}
                optional {?id skos:definition ?description}
                filter not exists {?id skos:narrower ?c}
                filter ( lang(?label) = "fr")
            }"""
        )
        return self.request_to_places(request)

    def get_from_keyword(self, keyword):
        """
        Query for the place with a label that contain the input keyword
        :param keyword: the keyword to search in labels
        :return the result of the request
        """
        request = self.graph.query(
            """
            select distinct ?label ?lat ?long ?bnf ?persee ?description
            where {
                ?id skos:prefLabel ?lab.
                ?id skos:altLabel ?altlab.
                ?id geo:lat ?lat.
                ?id geo:long ?long.
                ?id skos:broader+ ?b.
                filter not exists {?id skos:narrower ?c}
                filter(regex(?lab, '""" + keyword + """', "i") ||
                regex(?altlab, '""" + keyword + """', "i")).
                optional {?id skos:note ?bnf}
                optional {?id skos:editorialNote ?persee}
                optional {?id skos:definition ?description}
                ?id skos:prefLabel ?label.
                filter ( lang(?label) = "fr")
            }"""
        )
        return self.request_to_places(request)

    def request_to_places(self, request):
        """
        :param request: the result of the query
        :return: the list of elements with attributes that match the input query
        """
        places = []
        for row in request:
            bnf = re.findall(r"(?P<url>https?://[^\s]+)", str(row[3]))
            persee = re.findall(r"(?P<url>https?://[^\s]+)", str(row[4]))
            places.append(Place(row[0], row[1], row[2], bnf, persee, row[5]))
        return places

    def request(self, directory, keyword, lat, long, radius):
        """
        The main request, takes all parameters (can be None)
        :param directory: the category of places we looking for
        :param keyword: the keyword to search into labels
        :param lat: the latitude of the center of the circle
        :param long: the longitude of the center of the circle
        :param radius: the distance max from the middle of the circle
        :return: the list of places
        """
        result = set()

        if directory is not None:
            result = self.get_from_directory(directory)

        if keyword is not None:
            result_keyword = self.get_from_keyword(keyword)
            if len(result) == 0:
                result = result_keyword
            else:
                labels = set(place.name for place in result)
                result = [place for place in result_keyword if place.name in labels]
        if radius is not None and lat is not None and long is not None:
            result_location = self.get_from_location(lat, long, radius)
            if len(result) == 0:
                result = result_location
            else:
                labels = set(place.name for place in result)
                result = [place for place in result_location if place.name in labels]
        return result


if __name__ == '__main__':
    start_time = time.time()
    parser = ParserRdf("../../../cache/idg=MT62&th=42.xml")
    print("Temps d execution : %s secondes ---" % (time.time() - start_time))
    # places = parser.request("architecture civile", None, None, None, None)
    # places = parser.get_from_keyword("Latin")
    places = parser.request("jardins", "y", 30.09358, 31.32511, 10)
    print("Temps d execution : %s secondes ---" % (time.time() - start_time))
    for place in places:
        print(place.name)
