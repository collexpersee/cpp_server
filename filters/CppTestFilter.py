# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""

from __future__ import absolute_import
from qgis.server import QgsServerFilter
from .modules.commons.util import g_logger
from .modules.commons.request import Request
from .modules.facaderequest import FacadeRequest


def fill_request_test():
    import urllib

    my_request = Request()
    my_request.thesaurus = "OpenthesoHumaNum"
    my_request.thesaurus_url = urllib.unquote(
        'https%3A%2F%2Fopentheso.huma-num.fr%2Fopentheso%2Fwebresources%2Frest%2Fskos%2Fconcept%2Fall').decode('utf8')
    my_request.gazetteer = 'idg=MT62&th=42'
    my_request.keyword = 'y'
    my_request.longitude = 31.32511
    my_request.latitude = 30.09358
    my_request.wkt = urllib.unquote(
        'Point+%28274+-132%29%3BPolygon+%28%28259.36342+-114.53253'
        '%2C+269.83359+-121.86165%2C+257.26939+-146.99006'
        '%2C+274.37067+-172.81648%2C+242.96016+-153.27216%2C+259.36342+-114.53253%29%29').decode('utf8')
    my_request.radius = 10
    my_request.directory = 'jardins'
    my_request.validate()

    g_logger.debug(my_request.__str__())

    my_requestfacade = FacadeRequest(my_request)
    my_result_json = my_requestfacade.get_result()

    g_logger.debug(my_result_json)

    return my_result_json


class CppTestFilter(QgsServerFilter):
    """
    Classe filtre qui hérite de QgsServerFilter pour la manipulation des filtres URL
    :exemples:
        wget -q -O - "http://localhost:81/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=PLACETEST"
    """

    def __init__(self, server_iface):
        super(CppTestFilter, self).__init__(server_iface)
        g_logger.debug("CppTestFilter.__init__")
        pass

    def requestReady(self):
        g_logger.info("CppTestFilter.requestReady")
        pass

    def sendResponse(self):
        g_logger.info("CppTestFilter.sendResponse")
        pass

    def responseComplete(self):
        g_logger.info("CppTestFilter.responseComplete")
        request_handler = self.serverInterface().requestHandler()
        try:
            # pour tester le plugin
            if request_handler.parameter('SERVICE').upper() == 'CPP'\
                    and request_handler.parameter('REQUEST').upper() == 'FINDTEST':
                request_handler.clearHeaders()
                request_handler.setHeader('Content-type', 'text/plain')
                request_handler.clearBody()

                my_result_json = fill_request_test()

                request_handler.appendBody(my_result_json)
                import getpass
                g_logger.debug('user plugin : ' + getpass.getuser() + '\n')

        except Exception as e:
            smsg = 'Exception capturée :' + str(e)
            g_logger.info(smsg)
            request_handler.appendBody(smsg)
            request_handler.appendBody("\n")
