# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""

from __future__ import absolute_import
from qgis.server import QgsServerFilter
from qgis.server import QgsRequestHandler
from .modules.commons.util import g_logger, require
# from .modules.commons.request import Request
# from .modules.facaderequest import FacadeRequest
from .modules.commons.createdatabase import geojson_to_proj_qgs
import os


class NodeCppFilter(QgsServerFilter):
    """
    Classe filtre qui hérite de QgsServerFilter pour créer une  base de donnée depuis un GEOJSON envoyé par URL
    :exemples:
        wget -q -O - "http://localhost:81/cgi-bin/qgis_mapserv.fcgi?SERVICE=NODE&REQUEST=PUSH\
        &TH=opentheso&GAZETTEER=Modern Cairo Gazetteer"
        wget -q -O - "http://localhost:8080/?SERVICE=NODE&REQUEST=PUSH\
        &TH=opentheso&GAZETTEER=Modern Cairo Gazetteer"
    """

    def __init__(self, server_iface):
        super(NodeCppFilter, self).__init__(server_iface)
        g_logger.debug("NodeCppFilter.__init__")
        pass

    def requestReady(self):
        g_logger.info("NodeCppFilter.requestReady")
        request_handler = self.serverInterface().requestHandler()
        # pour tester le plugin
        if request_handler.parameter('SERVICE').upper() == 'WFS':
            g_logger.info("WFS ready before processing")
        pass

    def sendResponse(self):
        # commentaires
        g_logger.info("NodeCppFilter.sendResponse")
        pass

    def responseComplete(self):
        g_logger.info("NodeCppFilter.responseComplete")
        request_handler = self.serverInterface().requestHandler()
        try:
            # pour tester le plugin
            if request_handler.parameter('SERVICE').upper() == 'NODE'\
                    and request_handler.parameter('REQUEST').upper() == 'PUSH':
                request_handler.clearHeaders()
                request_handler.setHeader('Content-type', 'text/plain')
                request_handler.clearBody()
                r = self.push_in_database(request_handler)

                request_handler.appendBody(r)

        except Exception as e:
            smsg = 'Exception capturée :' + str(e)
            g_logger.info(smsg)
            request_handler.appendBody(smsg)
            request_handler.appendBody("\n")

    # refactoring function
    @require('request_handler', QgsRequestHandler)
    def push_in_database(self, request_handler):
        """
        refactoring method
        """
        import urllib
        thesaurus = urllib.unquote(request_handler.parameter('TH')).decode('utf8')
        # thesaurus_url = urllib.unquote(request_handler.parameter('URL')).decode('utf8')
        gazetteer = urllib.unquote(request_handler.parameter('GAZETTEER')).decode('utf8')
        # keyword = urllib.unquote(request_handler.parameter('KEYWORD')).decode('utf8')
        # to change
        # thesaurus = "opentheso"
        # to change
        # gazeteer = "cairo"
        # my_geojson = urllib.unquote(request_handler.parameter('GEOJS')).decode('utf8')

        path_geojson = os.getenv("HOME")+'/qgisserverdb/{}.geojson'.format(gazetteer)
        # in push method, where we want
        # to write files, we have blank folder the first time !
        if not os.path.exists(os.getenv("HOME")+'/qgisserverdb'):
            os.mkdir(os.getenv("HOME")+'/qgisserverdb')
        # First time: it creates the file, and other times it will write on it
        # f = open(path_geojson, 'w')
        # f.write(my_geojson)
        # f.close()
        # now we have to create DATABASE following Qgis spec.
        # the corresponding path to the new database created is returned
        end_path = geojson_to_proj_qgs(thesaurus, gazetteer, path_geojson)
        g_logger.debug('wrote database from ' + path_geojson + str(end_path))
        g_logger.debug(thesaurus + ' ' + gazetteer)
        return "PUSH effectué"
