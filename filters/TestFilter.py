# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from __future__ import absolute_import
from qgis.server import QgsServerFilter
from .modules.commons.util import g_logger


class TestFilter(QgsServerFilter):
    """
    classe de test pour le fonctionnement du plugin
    exemples: wget -q -O - "http://localhost:81/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=TEST"
    """
    def __init__(self, server_iface):
        super(TestFilter, self).__init__(server_iface)
        g_logger.debug("TestFilter.__init__")
        pass

    def requestReady(self):
        g_logger.info("TestFilter.requestReady")
        pass

    # test
    def sendResponse(self):
        g_logger.info("TestFilter.sendResponse")
        pass

    def responseComplete(self):
        g_logger.info("TestFilter.responseComplete")
        request = self.serverInterface().requestHandler()
        params = request.parameterMap()
        if params.get('SERVICE', '').upper() == 'CPP' and params.get('REQUEST', '').upper() == 'TEST':
            request.clearHeaders()
            request.setHeader('Content-type', 'text/plain')
            request.clearBody()
            request.appendBody('Test CPP plugin!')
