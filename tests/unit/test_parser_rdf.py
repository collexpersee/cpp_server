# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""
from filters.modules.commons.parser_rdf import ParserRdf
from filters.modules.commons.util import project_path


class TestParserRdf:

    def setup(self):
        self.parser = ParserRdf(project_path() + "tests/unit/resources/rdf_test.xml")

    def test_first_node(self):
        assert str(self.parser.get_first_node()) == "Modern Cairo Gazetteer"

    def test_all(self):
        assert str(self.parser.get_all()[0].name) == "place Atabah"

    def test_directory(self):
        assert self.parser.get_directories()["root"] == ["places"]

    def test_location(self):
        assert str(self.parser.request(None, None, 30.09358, 31.32511, 10)[0].name) == "place Atabah"

    def test_keyword(self):
        assert str(self.parser.request(None, "AtAbet", None, None, None)[0].name) == "place Atabah"
        assert str(self.parser.request(None, "place", None, None, None)[0].name) == "place Atabah"
