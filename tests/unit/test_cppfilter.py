# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""
import requests
import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')


class TestCppFilter:

    def test(self):
        if __name__ == '__main__':
            rq = requests.get('http://localhost/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=TEST')
            assert rq.content == 'Test CPP plugin!'
        pass

    def test_find(self):
        if __name__ == '__main__':
            rq = requests.get('http://localhost/cgi-bin/qgis_mapserv.fcgi?SERVICE=CPP&REQUEST=FINDTEST')
            assert rq.content == '[{"Persee_url": [], "Description": null, "long": "31.24798", "lat": "30.05169",' \
                                 ' "Bnf_url": [], "Name": "jardin de l\'Ezbekieh"}]'
        pass


if __name__ == '__main__':
    from filters.modules.commons.util import g_logger
    g_logger.init()
    t = TestCppFilter()
    t.test()
    t.test_find()
    print('test ok')
