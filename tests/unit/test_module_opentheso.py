# -*- coding: utf-8 -*-
"""
/***************************************************************************
 CppServer
                                 A QGIS Server plugin
 to do
                             -------------------
        begin                : 2019-03-12
        git sha              : $Format:%H$
        copyright            : (C) 2019 by A. Fondere, C. Crapart, E. Viegas,
            M. Permalnaick, S. Thaalbi, T. Harling, W. Podjelski, ENSG
        email                : collexpersee@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ************************************************it add***************************/
"""
from filters.modules.module_OpenthesoHumaNum import OpenthesoHumaNum
from filters.modules.commons.request import Request
from filters.modules.commons.util import g_logger
import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')


class TestModuleOpentheso:

    def setup(self):
        g_logger.init()
        self.request = Request()
        self.request.thesaurus = "OpenthesoHumaNum"
        self.request.gazetteer = None
        self.request.thesaurus_url = None
        self.request.directory = 'jardins'
        self.request.keyword = 'y'
        self.request.longitude = 31.32511
        self.request.latitude = 30.09358
        self.request.radius = 10
        self.request.wkt = None
        self.my_module = OpenthesoHumaNum()

    def test_connexion(self):
        self.my_module.connexion(self.request)
        pass

    def test_conversion(self):
        self.my_module.connexion(self.request)
        var = self.my_module.conversion()
        assert str(var.listName[0]) == "place Atabah"


if __name__ == '__main__':
    g_logger.init()
    t = TestModuleOpentheso()
    t.setup()
    t.test_connexion()
    t.test_conversion()
    print('test ok')
